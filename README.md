# LED Matrix
An HT16K33-based LED matrix I2C driver


## Introduction
This is a little I2C driver for the HT16K33-based LED matrix. It is written in Python and supports basic operations (e.g., set pixel, clear pixel, clear the matrix, display an image, etc.) as well as several animations (e.g., random walk or spectrum analyzer).

The driver consists of two classes: `Matrix`, the base class implemeting basic operations, and a subclass `MatrixMotion` implementing animations. The extra class `Img` is a container of "constants" representing 8x8 images in the HEX format.


## Usage Examples
Lit the first LED for one second using the lowest brightness:
```python
from led_matrix import Matrix
Matrix().lit(0).clr().px1(0,0).set().hold(1).px0(0,0).set()
```

Lit the second LED and cycle through the full brightness range of the display three times:
```python
from led_matrix import Matrix
m = Matrix()
m.clr().px1(1,0).set()
for _ in range(3):
    for i in Matrix.LIT_LST_BREATHE: m.lit(i).hold(0.01)
m.clr().set()
```

Play several animations (mass random points, random walk, spectrum analyzer, and image breating):
```python
from led_matrix import Img, Matrix, MatrixMotion
mm = MatrixMotion()
mm.points_rand(16, n_rep=15, lit_max=Matrix.LIT_MIN, t_frame=0.1, t_wait=0.025, t_post=0.25)
mm.points_rand_walk( 3, n_rep=50,lit_max=Matrix.LIT_MIN, t_frame=0.05, t_wait=0.025, t_post=0.25)
mm.spectrum_analyzer(n_rep= 75, lit_max=Matrix.LIT_MIN, t_pre=0.5, t_frame=0.035, t_wait=0.01, t_post=0.5)
mm.image(Img.FACE_U_S, 3, 1, t_pre=0.75, t_post=0.75, t_frame=0.005, t_wait=0.05)
```

Have a look at the bottom of `led8x8.py` for more examples.  [LED Tetris](https://github.com/ubiq-x/led_tetris) is an example of a program using this driver (inside of a thread).


## Dependencies
- [smbus2](https://github.com/kplindegaard/smbus2)


## Setup
### Hardware
You will need the display hooked up to the I2C pins on your single-board computer.  I've been using the Adafruit Mini 8x8 LED Matrix w/I2C Backpack on the Raspberry Pi 3 and the 0W, but I suspect it will work with other boards as well.

### Software
First, initialize a new Python venv and install the requirements:
```sh
venv=led_matrix
python=python3.6

mkdir -p ~/prj/
cd ~/prj
$python -m venv $venv
cd $venv
source ./bin/activate

python -m pip install smbus2

mkdir src
cd src
git clone https://github.com/ubiq-x/led_matrix
```

Then, run all the test like so:
```sh
cd led_matrix
python led8x8.py
```


## Final Result
Here's an example of a matrix displaying an image:
![Example of an image displayed on the matrix](media/led-02.jpg)
Have a look at the wiki for more photos.


## Acknowledgments
I've used the excellent [LED Matrix Editor](https://xantorohara.github.io/led-matrix-editor) to get the HEX images.  Go check it out if you haven't already.  It kicks ass!


## License
This project is licensed under the [BSD License](LICENSE.md).
